import Links from "./Links";
import { connect } from "react-redux";

function mapStateToProps({ details }) {
  return {
    links: details.links
  };
}

export default connect(mapStateToProps)(Links);
