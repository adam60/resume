import React from 'react';
import { BulletContainer, BulletTitle } from './styled';

const BulletRow = ({ title, value }) => {
  return (
    <BulletContainer>
      <BulletTitle>{title}:</BulletTitle> {value}
    </BulletContainer>
  );
};

export default BulletRow;
