import React from 'react';
import { SectionTitle } from '../styled';
import { LinkTitle } from './styled';

const Links = ({ links }) => {
  const linkList = links.map((l, i) => (
    <li key={`link_${i}`}>
      <LinkTitle>{l.title}:</LinkTitle>
      <a href={`https://${l.url}`} rel="noopener noreferrer" target="_blank">
        {l.url}
      </a>
    </li>
  ));
  return (
    <div>
      <SectionTitle>Links</SectionTitle>
      <ul>{linkList}</ul>
    </div>
  );
};

export default Links;
