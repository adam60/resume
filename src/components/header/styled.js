import styled from 'styled-components';
import { H1, H2, Container } from '../styled';

export const NameDisplay = H1;

export const AddressDisplay = H2;

export const ContactDisplay = H2;

export const HeaderContainer = styled(Container)`
  margin-top: 0;
  text-align: center;
`;
