import initialState from "../data/details";

function details(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default details;
