import Highlights from "./Highlights";
import { connect } from "react-redux";

function mapStateToProps({ details }) {
  return {
    teamHighlights: details.teamHighlights,
    projectHighlights: details.projectHighlights
  };
}

export default connect(mapStateToProps)(Highlights);
