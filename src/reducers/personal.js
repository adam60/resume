import initialState from "../data/personal";

function personal(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default personal;
