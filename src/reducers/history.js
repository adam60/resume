import initialState from "../data/history";

function history(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default history;
