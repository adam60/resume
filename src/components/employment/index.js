import { connect } from 'react-redux';
import EmployementList from './EmploymentList';

function mapStateToProps({ history }) {
  return {
    history: history,
  };
}

export default connect(mapStateToProps)(EmployementList);
