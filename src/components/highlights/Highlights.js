import React from 'react';
import { Container, SectionTitle } from '../styled';
import { HighlightHeader, HighlightList, HighlightListItem } from './styled';

const Highlights = ({ projectHighlights, teamHighlights }) => {
  const projectHighlightList = projectHighlights.map((h, i) => (
    <HighlightListItem key={`project_highlight_${i}`}>{h}</HighlightListItem>
  ));
  const teamHighlightList = teamHighlights.map((h, i) => (
    <HighlightListItem key={`project_highlight_${i}`}>{h}</HighlightListItem>
  ));
  return (
    <Container>
      <SectionTitle>Things I've Done</SectionTitle>
      <HighlightHeader>Team Highlights</HighlightHeader>
      <HighlightList>{teamHighlightList}</HighlightList>
      <HighlightHeader>Project Highlights</HighlightHeader>
      <HighlightList>{projectHighlightList}</HighlightList>
    </Container>
  );
};

export default Highlights;
