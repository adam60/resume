import React from 'react';

import BulletRow from './BulletRow';
import {
  EmployerContainer,
  EmployerHeader,
  EmployerPosition,
  Filler,
} from './styled';

const EmploymentItem = props => {
  const { company, position, from, to, listKey } = props;
  const { summary, technology, additional, frameworks } = props;
  const additionalBullets =
    additional && additional.length > 0
      ? additional.map((add, i) => (
          <BulletRow
            key={`additional_bullet_${listKey}_${i}`}
            title={add.key}
            value={add.value}
          />
        ))
      : null;
  return (
    <EmployerContainer>
      <EmployerHeader>
        <div>{company}</div>
        <EmployerPosition>, {position}</EmployerPosition>
        <Filler />
        <div>
          {from}-{to || 'current'}
        </div>
      </EmployerHeader>
      <BulletRow title="Summary" value={summary} />
      {additionalBullets}
      <BulletRow title="Technology" value={technology.join(', ')} />
      <BulletRow title="Frameworks/Libraries" value={frameworks.join(', ')} />
    </EmployerContainer>
  );
};

export default EmploymentItem;
