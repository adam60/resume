import { combineReducers } from "redux";
import personal from "./personal";
import history from "./history";
import details from "./details";
export default combineReducers({ personal, history, details });
