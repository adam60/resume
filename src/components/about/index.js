import About from "./About";
import { connect } from "react-redux";

function mapStateToProps({ details }) {
  return {
    about: details.about
  };
}

export default connect(mapStateToProps)(About);
