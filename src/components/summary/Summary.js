import React from "react";
import { SectionTitle } from "../styled";

const Summary = ({ summary }) => {
  return (
    <div>
      <SectionTitle>Summary</SectionTitle>
      {summary}
    </div>
  );
};

export default Summary;
