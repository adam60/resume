import Summary from "./Summary";
import { connect } from "react-redux";

function mapStateToProps({ details }) {
  return {
    summary: details.summary
  };
}

export default connect(mapStateToProps)(Summary);
