export const colors = {
  h1: 'rgb(43.6%, 51.9%, 62.4%)',
  accent: 'rgb(58.1%, 7.2%, 7.2%)',
  text: 'rgb(15.5%, 15.4%, 14.9%)',
  lighten: 'rgb(31.6%, 31.6%, 31.6%)',
};
