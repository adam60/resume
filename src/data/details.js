export default {
  summary:
    "I engineer mobile and web applications with focus on front end development.  I have served as architect, programmer, and team lead on projects. My primary language is JavaScript. I am a linux user.  I love React.  I have built projects with Angular, Meteor, Aurelia, Durandal, Backbone, Ember, and others along with vanilla JS.  On the back end, I have a long history with C#, but really enjoy building APIs with NodeJS.  When leveraging C# I prefer working with Dotnet Core.",
  teamHighlights: [
    "Directed an offshore team as an application architect to rebuild a mobile sales application",
    "Established and lead remote teams on development projects.",
    "Acting Scrum Master for a small team",
    "Mentored junior level developers",
    "Managed clients as primary point of contact"
  ],
  projectHighlights: [
    "Worked on mobile ecommerce application with a strong focus on client side architecture.",
    "Built a patient check-in kiosk application for immediate care centers.",
    "Planned, developed, and delivered a classroom management web application with companion native android application for attendance taking.",
    "Created a small social networking site for my friends to stay in touch.",
    "Developed a smart tv application for renting and streaming videos.",
    "Wrote a web API for native IPad training application.",
    "Created an exam creation tool within the faculty portal, and an exam taking application for the student portal.",
    "Developed a SalesForce Canvas application for building dynamic contact forms.",
    "Built an intranet site for a global company with over 30,000 users."
  ],
  about:
    "I am interested in JavaScript projects on server and client side.  I prefer to develop on linux machines, and I appreciate it when I can bring/choose my own hardware. I’m very interested in automotive, aerospace, and clean energy spaces. I am no stranger to remote work. It’s not for everyone, but it is for me. I have heavily invested in my home office.  I also lease office space as an alternative location.",
  links: [
    { title: "Blog", url: "adam.tumminaro.net/blog" },
    { title: "Codepen", url: "codepen.io/sweatercomeback" },
    { title: "Github", url: "github.com/sweatercomeback" },
    { title: "StackOverflow", url: "stackoverflow.com/users/4417964/ship-b" }
  ]
};
