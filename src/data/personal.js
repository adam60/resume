export default {
  firstName: "Adam",
  lastName: "Tumminaro",
  address1: "337 S. Prospect St",
  city: "Roselle",
  state: "IL",
  postalCode: "60172",
  phone: "630.670.3012",
  email: "adam@tumminaro.com"
};
