import React, { Component } from 'react';
import {
  HeaderContainer,
  NameDisplay,
  AddressDisplay,
  ContactDisplay,
} from './styled';

export default class Header extends Component {
  render() {
    const { name, address, phone, email } = this.props;
    return (
      <HeaderContainer>
        <NameDisplay>{name}</NameDisplay>
        <AddressDisplay>{address}</AddressDisplay>
        <ContactDisplay>
          {phone} · {email}
        </ContactDisplay>
      </HeaderContainer>
    );
  }
}
