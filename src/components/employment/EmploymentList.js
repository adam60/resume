import React, { Component } from 'react';
import EmploymentItem from './EmploymentItem';

import { MainList, EmploymentContainer } from './styled';
import { SectionTitle } from '../styled';

export default class EmploymentList extends Component {
  render() {
    const items = this.props.history.map((historyItem, i) => (
      <EmploymentItem
        {...historyItem}
        listKey={`employment_item_${i}`}
        key={`employment_item_${i}`}
      />
    ));
    return (
      <EmploymentContainer>
        <SectionTitle>Employment Highlights</SectionTitle>
        <MainList>{items}</MainList>
      </EmploymentContainer>
    );
  }
}
