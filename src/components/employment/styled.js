import styled from 'styled-components';
import { List, ListItem, H2, Container } from '../styled';
import { colors } from '../styled/variables';

export const MainList = styled(List)``;

export const EmploymentContainer = styled(Container)`
  max-width: 75%;
  text-align: left;
`;

export const EmployerContainer = styled(ListItem)`
  margin: 0.7em 0;
`;

export const EmployerHeader = styled(H2)`
  display: flex;
  justify-content: space-between;
  font-size: 1.2em;
  color: ${colors.accent};
  align-items: flex-end;
`;

export const Filler = styled.div`
  flex-grow: 1;
  text-align: center;
`;

export const EmployerPosition = styled.div`
  color: ${colors.text};
  font-size: 0.9em;
`;

export const BulletContainer = styled.div`
  margin: 0.25em 1em;
`;

export const BulletTitle = styled.strong`
  margin-right: 0.5em;
  color: ${colors.lighten};
`;
