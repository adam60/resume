export default [
  {
    company: "Dealer Inspire",
    position: "Product Developer",
    from: "August 2018",
    to: null,
    summary:
      "Developer on a product team building an embedded online vehicle shopping application",
    technology: [
      "JavaScript",
      "Docker",
      "PHP",
      "HTML",
      "CSS (SCSS)",
      "MySQL",
      "Git",
      "NodeJS"
    ],
    frameworks: ["React", "Redux"]
  },
  {
    company: "Ship B, LLC",
    position: "Independent Contractor",
    from: "September 2013",
    to: null,
    summary:
      "My LLC that I run consulting projects through. I am the sole member and I wear a lot of hats.",
    additional: [
      {
        key: "Clients",
        value:
          "Gladson, Practice Velocity, Ticketmaster Resale, California Forward, Market 6, Gate 3"
      }
    ],
    technology: ["JavaScript"],
    frameworks: [
      "React",
      "Aurelia",
      "Angular",
      "Meteor",
      "NativeScript",
      "Dotnet Core",
      ".Net MVC"
    ]
  }
];
