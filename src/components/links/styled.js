import styled from 'styled-components';
import { colors } from '../styled/variables';

export const LinkTitle = styled.strong`
  margin-right: 0.5em;
  color: ${colors.lighten};
`;
