import React, { Component } from "react";
import Header from "../header";
import Summary from "../summary";
import EmploymentList from "../employment";
import Highlights from "../highlights";
import About from "../about";
import Links from "../links";

export default class Resume extends Component {
  render() {
    return (
      <div>
        <Header />
        <Summary />
        <EmploymentList />
        <Highlights />
        <About />
        <Links />
      </div>
    );
  }
}
