import React from "react";
import { SectionTitle } from "../styled";

const About = ({ about }) => {
  return (
    <div>
      <SectionTitle>Things I Like</SectionTitle>
      {about}
    </div>
  );
};

export default About;
