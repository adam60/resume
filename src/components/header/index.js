import Header from "./Header";
import { connect } from "react-redux";

function mapStateToProps({ personal }) {
  return {
    name: `${personal.firstName} ${personal.lastName}`,
    address: `${personal.address1}, ${personal.city}, ${personal.state} ${
      personal.postalCode
    }`,
    email: personal.email,
    phone: personal.phone
  };
}

export default connect(mapStateToProps)(Header);
