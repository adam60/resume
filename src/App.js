import React, { Component } from "react";

import Resume from "./components/resume";

import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Resume />
      </div>
    );
  }
}

export default App;
