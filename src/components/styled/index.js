import styled from 'styled-components';
import { colors } from './variables';

export const H1 = styled.h1`
  margin: 0;
  padding: 0;
  font-weight: 500;
`;

export const H2 = styled.h2`
  margin: 0;
  padding: 0;
  font-size: 1.3em;
  font-weight: 300;
`;

export const H3 = styled.h2`
  margin: 0;
  padding: 0;
  font-size: 1.3em;
  font-weight: 300;
`;

export const Container = styled.div`
  margin: 1em 0;
`;

export const SectionTitle = styled(H1)`
  color: ${colors.h1};
`;

export const List = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
`;

export const ListItem = styled.li`
  margin: 0;
  padding: 0;
`;
