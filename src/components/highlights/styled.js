import styled from 'styled-components';
import { colors } from '../styled/variables';

export const HighlightHeader = styled.h3`
  margin: 0.2em 1em;
  padding: 0;
  color: ${colors.lighten};
`;

export const HighlightList = styled.ul`
  margin-top: 0;
`;
export const HighlightListItem = styled.li``;
